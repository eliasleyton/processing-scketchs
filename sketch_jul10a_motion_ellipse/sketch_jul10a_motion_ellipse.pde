import seltar.motion.*;

int cantPuntos = 500;
Motion m;

void setup () {
  size (1080,720); //HD
  background(#FFFFFF,50);  
  colorMode(HSB,100);
  smooth();  
  
  m = new Motion(random(width),random(height));
  frameRate(64);  
  m.setConstant(10);
  noFill();
}

int cont = 0;
float goy, gox;

void draw () {
  //fill(#FFFFFF,5);
  //rect(0,0,width,height);

  m.followTo(gox,goy);
  m.move();
  m.move();
  m.move();
  noFill();
  ellipse(m.getX(), m.getY(),m.getDistance()*100,m.getDistance()*100);
  if(m.getDistance() < 0.1){
    gox = random(width);
    goy = random(height);
    filter(BLUR,1);
  }
  
  
}

//void mouseClicked() {
  //gox = random(width);
  //goy = random(height);
  
  //gox = mouseX;
  //goy = mouseY;
//}
