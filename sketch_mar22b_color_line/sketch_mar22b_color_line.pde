void setup(){
  size(ancho,alto);
  stroke(#9B8300);
  background(#FFFFFF);
  smooth();
  frameRate(10);
}

int alto = 500;
int ancho = 500;

float x1,y1,x2,y2;

float xoff = 0.0;

int i = 0;
void draw () {
  for(int i = 0; i < alto; i++){
     xoff += 0.1;
     x1 = noise (xoff) * random(200);
     y1 = i * 3;
     y2 = y1;
     x2 = random (150,200);
     stroke(#FF12C4);
     line(0,y1,random(10)+x1,y2);
     stroke(#C2C716);
     line(10+x1,y1, x2,y2);
     stroke(#6CD41C);
     float temp = random(50,100);
     line(10+x2,y1,x2+temp,y2);
     stroke(#81D8E3);
     line (x2+temp+10,y1,ancho,y2);
 }
 noStroke();
 fill(#FFFFFF);
 rect(ancho/2-50,alto/2,100,20);
 noLoop();
}
