PImage a;


void setup (){
  size(1000,600);  
  background(#FFFFFF);
  
  a = loadImage("image1.jpg");
}

void draw () {
  image(a, 0, 0);
  motivo ();
  filter(BLUR,0.5);
  noLoop();
  
}

void motivo () {
  stroke(#000000);
  for (int i = 0; i < width; i = i + 2){
    for (int j = 0; j < height; j = j + 2){
      point(i,j);
    }
  }   
}


