void setup () {
  size (800,600);
  smooth();
  //frameRate(23);
}

float x = 400.0;
float y = 300.0;

void draw () {
  noStroke();
  //background(200);
  x = ruido(x);
  y = ruido(y);
  ellipse(x,y,20,20);
}

float ruido(float x) {
  return x+random(-10,10);
 
}

float ruido2(float x) {
  return x+random(-2,2);
 
}
