void setup () {
  size (600,600);
  smooth();
  noFill();
}

float r = 100;
int l = 12;
float grad = 360/l;

void draw () {
  ellipse(300,300,200,200);
  translate(300,300);
  for(int i = 1; i <= l; i++){
    line(0,0,r*sin(radians(grad)*i),r*cos(radians(grad)*i));
    ellipse(r*sin(radians(grad)*i),r*cos(radians(grad)*i),10,10);
  }
  
  
//  translate(300,300);
  noLoop();
}

