import processing.core.*; 
import processing.xml.*; 

import seltar.motion.*; 

import java.applet.*; 
import java.awt.Dimension; 
import java.awt.Frame; 
import java.awt.event.MouseEvent; 
import java.awt.event.KeyEvent; 
import java.awt.event.FocusEvent; 
import java.awt.Image; 
import java.io.*; 
import java.net.*; 
import java.text.*; 
import java.util.*; 
import java.util.zip.*; 
import java.util.regex.*; 

public class sketch_jul11a_magnetV2_1 extends PApplet {



int cant = 10000;
Motion [] m = new Motion [cant];
PVector [] p = new PVector [cant];
float d;
public void setup () {
  size (950,450);
    
  
  frameRate(160);
  noFill();
  
  for(int i = 0; i < cant; i++){
    
    m[i] = new Motion(random(width),random(height));
    m[i].setConstant(10);
    p[i] = new PVector(m[i].getX(),m[i].getY());
  }
}

public void draw (){
  background(10);
  for(int i = 0; i < cant; i++){
    m[i].followTo(mouseX,mouseY);
    //m[i].springTo(mouseX,mouseY);
    float dista = random(random(400));
    if(p[i].dist(new PVector(mouseX,mouseY)) <= dista) {
      m[i].move();
      
      p[i].x = m[i].getX();
      p[i].y = m[i].getY();
      if(p[i].dist(new PVector(mouseX,mouseY)) <= 20) {
        m[i].setX(random(width));
        m[i].setY(random(height));
      }
    }
    d = p[i].dist(new PVector(mouseX,mouseY))*0.005f;
    if (d > 1){
      d = 1;
    }
    //println(d);
    stroke(lerpColor(0xffFF00EF,0xff00DBFF,d));
    point(m[i].getX(),m[i].getY());
      
}
}

  static public void main(String args[]) {
    PApplet.main(new String[] { "--bgcolor=#ECE9D8", "sketch_jul11a_magnetV2_1" });
  }
}
