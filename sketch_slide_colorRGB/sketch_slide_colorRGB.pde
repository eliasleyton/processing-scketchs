import controlP5.*;
ControlP5 sliders;

float r = 255.0;
float g = 128.0;
float b = 0.0;
float a = 200.0;

void setup() {
  size(450,200);
  //background(#999999); 
  sliders = new ControlP5(this);
  sliders.addSlider("r",0,255,r,250,50,10,100);
  //Slider s1 = (Slider) sliders.controller("r");
  //s1.setNumberOfTickMarks(10);
  sliders.addSlider("g",0,255,g,300,50,10,100);
  //Slider s2 = (Slider) sliders.controller("g");
  //s2.setNumberOfTickMarks(10);
  sliders.addSlider("b",0,255,b,350,50,10,100);
  //Slider s3 = (Slider) sliders.controller("b");
  //s3.setNumberOfTickMarks(10);
  sliders.addSlider("a",0,255,a,400,50,10,100);
  
}

void draw () {
  background(#555555); 
  fill(r,g,b,a);
  rect(50,50,190,100);
}
