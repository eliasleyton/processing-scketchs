import pitaru.sonia_v2_9.*;
import SoniaHelper.*;
import fullscreen.*; 

FullScreen fs; 
int spectrumLength=256; // Needs to be power of 2
int bandsteps;
float maxdist,damperval;
SoniaHelper ffthelper;
float pt[];

void setup () {
  size(800,600);
  Sonia.start(this);
  frameRate(10);
  //smooth();
    fs = new FullScreen(this); 
  
  // enter fullscreen mode
  fs.enter(); 
  
  LiveInput.start(spectrumLength);
//  LiveInput.useEnvelope(true,1f);
  LiveInput.useEqualizer(true);  

  // SoniaHelper(int _n, int _nbands,boolean doAvg)
  ffthelper=new SoniaHelper(spectrumLength,32,false);
  ffthelper.setMaxLimits(200,2000);
  damperval=0.1f;
  ffthelper.setDamper(damperval);  
  
  pt=new float[spectrumLength*2];
  for(int i=0; i<spectrumLength; i++) {
    pt[i*2]=random(width-50)+25;
    pt[i*2+1]=random(height-50)+25;
  }
  bandsteps=spectrumLength/ffthelper.band.length;

  // Get the maximum distance the max value will travel.
  // Note that the max, maxMaximum and maxMinimum fields are
  // doubles. We need to cast them to float to use them here.
  maxdist=ffthelper.maxMaximum-ffthelper.maxMinimum;
}
int [] bit8  = {0, 51, 102, 153, 204, 255};
void draw() {
  float rad;

  background(bit8[(int)(random(0,5))],bit8[(int)(random(0,5))],bit8[(int)(random(0,5))]);
  noStroke();
  
  doSoundInput();

  for(int i=0; i<spectrumLength; i++) {
     
  //noStroke();
  fill(bit8[(int)(random(0,5))],bit8[(int)(random(0,5))],bit8[(int)(random(0,5))]);
     
     // scale rad according to spectrum value, using
     // cubic interpolation to give organic values.
     rad=2+50*(ffthelper.spectrum[i]*ffthelper.spectrum[i]);     
     ellipse(pt[i*2],pt[i*2+1], rad,rad); 
  }
  
  // show damper value
  fill(0,100,200);
  rect(660,0, 30,1);
  rect(660,0+height*damperval, 30,1);
  rect(660,height-1, 30,1);
}

void mouseDragged() {
 mousePressed(); 
}

void mousePressed() {
    damperval=(float)mouseY/(float)height;
    if(damperval>1) damperval=1;
    else if(damperval<0) damperval=0;
    ffthelper.setDamper(damperval);  
    println("damperval "+damperval);
}

public void doSoundInput() {
  LiveInput.getSpectrum();
  ffthelper.update(LiveInput.spectrum);
}
