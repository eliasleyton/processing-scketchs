import processing.core.*; 
import processing.xml.*; 

import seltar.motion.*; 

import java.applet.*; 
import java.awt.Dimension; 
import java.awt.Frame; 
import java.awt.event.MouseEvent; 
import java.awt.event.KeyEvent; 
import java.awt.event.FocusEvent; 
import java.awt.Image; 
import java.io.*; 
import java.net.*; 
import java.text.*; 
import java.util.*; 
import java.util.zip.*; 
import java.util.regex.*; 

public class sketch_jul11a_magnet extends PApplet {



int cant = 10000;
Motion [] m = new Motion [cant];
PVector [] p = new PVector [cant];

public void setup () {
  size (950,480);
    
  
  frameRate(164);
  noFill();
  
  for(int i = 0; i < cant; i++){
    
    m[i] = new Motion(random(width),random(height));
    m[i].setConstant(random(50.100f));
    p[i] = new PVector(m[i].getX(),m[i].getY());
  }
}

public void draw (){
  background(0xffFFFFFF);
  for(int i = 0; i < cant; i++){
    m[i].followTo(mouseX,mouseY);
    float dista = random(random(400));
    if(p[i].dist(new PVector(mouseX,mouseY)) <= dista) {
      m[i].move();
      p[i].x = m[i].getX();
      p[i].y = m[i].getY();
      if(p[i].dist(new PVector(mouseX,mouseY)) <= 10) {
        m[i].setX(random(width));
        m[i].setY(random(height));
      }
    }
    point(m[i].getX(),m[i].getY());
  }  
}



  static public void main(String args[]) {
    PApplet.main(new String[] { "--bgcolor=#ECE9D8", "sketch_jul11a_magnet" });
  }
}
