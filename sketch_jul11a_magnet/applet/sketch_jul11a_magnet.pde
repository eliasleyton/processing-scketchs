import seltar.motion.*;

int cant = 10000;
Motion [] m = new Motion [cant];
PVector [] p = new PVector [cant];

void setup () {
  size (950,480);
    
  
  frameRate(164);
  noFill();
  
  for(int i = 0; i < cant; i++){
    
    m[i] = new Motion(random(width),random(height));
    m[i].setConstant(random(50.100));
    p[i] = new PVector(m[i].getX(),m[i].getY());
  }
}

void draw (){
  background(#FFFFFF);
  for(int i = 0; i < cant; i++){
    m[i].followTo(mouseX,mouseY);
    float dista = random(random(400));
    if(p[i].dist(new PVector(mouseX,mouseY)) <= dista) {
      m[i].move();
      p[i].x = m[i].getX();
      p[i].y = m[i].getY();
      if(p[i].dist(new PVector(mouseX,mouseY)) <= 10) {
        m[i].setX(random(width));
        m[i].setY(random(height));
      }
    }
    point(m[i].getX(),m[i].getY());
  }  
}


