import controlP5.*;

ControlP5 controles;

int ancho = 950;
int alto = 500;
float radio = 100;
int lados = 5;
float grad = 360/lados;
float rotar = 0;
boolean check = false;

void setup (){
  size (ancho,alto);
  smooth();
  controles = new ControlP5(this); 
  controles.addSlider("radio",20,240,100,10,10,200,10);
  controles.addSlider("lados",3,24,5,10,25,200,10);
  Slider l_ = (Slider) controles.controller("lados");
  l_.setNumberOfTickMarks(22);
  controles.addSlider("rotar",0,360,0,10,48,200,10);
  controles.addTextlabel("mensaje","Creado por Elias Leyton ",10,70);
  frameRate(64);
  
  
}

void draw () {
  
  PVector [] v = new PVector [lados+1] ;
  
  grad = 360/lados;
  stroke(#333333);
  background(#999999);
  pushMatrix();
  translate(ancho/2,alto/2);
  rotate(radians(rotar));

//  v[2] = new PVector(100, 50);
//  println(v[2].x);
  

  for(int i = 0; i < lados; i++){
    v[i] = new PVector (radio*sin(radians(grad)*(1+i)),radio*cos(radians(grad)*(1+i)));
    v[i+1] = new PVector (radio*sin(radians(grad)*(2+i)),radio*cos(radians(grad)*(2+i)));
    line(v[i].x,v[i].y,v[i+1].x,v[i+1].y);
    //line(0,0,radio*sin(radians(grad)*i),radio*cos(radians(grad)*i));
    println("r_"+radio);
    println("l_"+lados);
  }
  if (check == true) {
    for (int i = 0; i < v.length; i++){
      for (int j = 1; j < v.length; j++){
       line(v[i].x,v[i].y,v[j].x,v[j].y);
      }
    } 
  }
  line(v[lados].x,v[lados].y,radio*sin(radians(grad)*1),radio*cos(radians(grad)*1));
  popMatrix();
}

void radios () {
    
}

