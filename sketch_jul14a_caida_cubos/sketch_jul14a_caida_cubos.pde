import seltar.motion.*;

int cant = 28;
int cantL = 4;


Motion [] m = new Motion [cant*cantL];

void setup (){
  size (950,500);  
  smooth();
  float d = (width-10)/cant;
  for (int i = 0; i < cant*cantL; i++){
    m[i] = new Motion(i*d+5,random(height/2));   
    m[i].setConstant(random(10,50));  
  }
  fill(#000000);

}

void draw () {
  background(#FFFFFF,10);
  for (int i = 0; i < cant*cantL; i++){
    m[i].move();
    rect(m[i].getX(),m[i].getY(),10,10);
    m[i].followTo(m[i].getX(),height-10);
    if (m[i].getY() >= height-11) {
      m[i].setY(0);      
    }
  }
  
}

